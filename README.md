## 免翻墙镜像
本页面收集了墙内镜像。请将此页加为书签。若镜像被封锁，请访问本页面获取最新镜像。
* Google 搜索：http://chetxia.co
* Google 搜索：http://luxtarget.co
* Google 搜索：http://1kapp.co
* Google 搜索：http://sinaapp.co
* Google 搜索：https://s3-us-west-2.amazonaws.com/google2/index.html
* Google 搜索：https://s3-us-west-1.amazonaws.com/google3/index.html
* Google 搜索：https://s3-eu-west-1.amazonaws.com/google4/index.html
* Google 搜索：https://s3-ap-northeast-1.amazonaws.com/google5/index.html
* Google 搜索：https://s3-ap-southeast-2.amazonaws.com/google6/index.html
* Google 搜索：https://s3-sa-east-1.amazonaws.com/google7/index.html
* Google 搜索：https://s3-ap-southeast-1.amazonaws.com/google.cn/index.html
* Google 搜索：https://s3.amazonaws.com/google./index.html
* 自由微博：https://s3-ap-southeast-1.amazonaws.com/freeweibo2/index.html
* 自由微博：https://s3.amazonaws.com/freeweibo./index.html
* 中国数字时代：https://s3-ap-southeast-1.amazonaws.com/cdtimes2/index.html
* 中国数字时代：https://s3.amazonaws.com/cdtimes./index.html
* 泡泡（未经审查的网络报道）：https://s3-ap-southeast-1.amazonaws.com/pao-pao2/index.html
* 泡泡（未经审查的网络报道）：https://s3.amazonaws.com/pao-pao/index.html
* 蓝灯(Lantern)以及自由微博和GreatFire.org官方中文论坛：https://lanternforum.greatfire.org

<img src="https://raw.githubusercontent.com/greatfire/z/master/logos.gif" />

## 订阅 email
* <a href="https://b.us7.list-manage.com/subscribe?u=854fca58782082e0cbdf204a0&id=c78949b93c">自由微博和GreatFire.org邮件订阅</a>
    
## 新闻
RT @ghoulr: 我去，居然有一个来自哈工大的曲径企业版申请，翻墙目的是科研，这，校长已经把自己人坑成马了啊 (2014年06月18日 12:07)
 ---
《苹果日报》港台网站被黑客攻击无法登录 <a href="http://www.nanzao.com/sc/hk/29897/ping-guo-ri-bao-gang-tai-wang-zhan-bei-hei-ke-gong-ji-wu-fa-deng-lu">www.nanzao.com/sc/hk/29897/ping-guo-ri-bao-gang-tai-wang-zhan-bei-hei-ke-gong-ji-wu-fa-deng-lu</a> 今天报纸上有这个 <a href="https://twitter.com/GreatFireChina/status/479112913333288960/photo/1">twitter.com/GreatFireChina/status/479112913333288960/photo/1</a> (2014年06月18日 12:06)
 ---
Chinese-Made Smartphone Comes With Spyware, Security Firm Says <a href="http://blogs.wsj.com/digits/2014/06/17/chinese-made-smartphone-comes-with-spyware-security-firm-says/">blogs.wsj.com/digits/2014/06/17/chinese-made-smartphone-comes-with-spyware-security-firm-says/</a> (2014年06月18日 06:01)
 ---
Want to make your very own FreeGoogle? Fully accessible in China w/o the use of circumvention tools? Now you can: <a href="https://github.com/greatfire/freegoogle">github.com/greatfire/freegoogle</a> (2014年06月17日 17:41)
 ---
RT @RTKcn: 21+ RT @kgen: 这次 Google 事件发生后，国内的反应，这图真是太精准了！ <a href="https://twitter.com/kgen/status/478433106450325504/photo/1">twitter.com/kgen/status/478433106450325504/photo/1</a> (2014年06月17日 02:52)
 ---
RT @conradhackett: Press Freedom 2014:
1 Finland
11 Estonia
18 Canada
33 UK
46 US
47 Haiti
148 Russia
154 Turkey
164 S Arabia
175 China htt… (2014年06月16日 16:31)
 ---
打败互联网审查，打败GFW－－Google镜像背后的秘密 <a href="https://s3-ap-southeast-2.amazonaws.com/google6/defeat-gfw.html">s3-ap-southeast-2.amazonaws.com/google6/defeat-gfw.html</a> (2014年06月16日 15:45)
 ---
Just desserts! U messed w/ karma when U took @FreeWeibo down from app store: China cracks down on Apple <a href="http://www.techinasia.com/china-cracks-apples-imessage-national-web-cleanup-continues/">www.techinasia.com/china-cracks-apples-imessage-national-web-cleanup-continues/</a> h/t @niubi (2014年06月16日 13:04)
 ---
《苹果日报》称北京迫使大客户撤广告 <a href="http://cn.nytimes.com/china/20140612/c12chinamedia/">cn.nytimes.com/china/20140612/c12chinamedia/</a> (2014年06月15日 12:27)
 ---
RT @hnjhj: 长期以来，网友们对于祸国殃民的GFW无非抱以两种心态：或望墙心叹或找办法绕开。暂时还没有专门机构通过研究和利用GFW的规则对其发起主动攻势的。大胆猜测，这将是接下来的趋势。 (2014年06月15日 03:38)
 ---
RT @JulieMakLAT: China's crackdown on Google services now 2 weeks old and counting. <a href="http://www.latimes.com/business/la-fi-0614-google-china-20140614-story.html">www.latimes.com/business/la-fi-0614-google-china-20140614-story.html</a> (2014年06月14日 14:15)
 ---
RT @phuslu: 【转】Google这次是绝对要彻底的say bye了。连大楼都不租了。据说要撤到一个小office。目前Google AJAX CDN，Google Ads这些无关痛痒的网址也都沦陷了。貌似只剩下一个analytics的js没被墙。http://t.co… (2014年06月14日 02:43)
 ---
New outbreak in guerrilla war Internet activists have been waging against “The Great Firewall” <a href="http://www.csmonitor.com/World/Asia-Pacific/2014/0613/A-great-workaround-for-China-s-Great-Firewall">www.csmonitor.com/World/Asia-Pacific/2014/0613/A-great-workaround-for-China-s-Great-Firewall</a> via @peterfordcsm (2014年06月13日 20:35)
 ---
RT @Edourdoo: Chinese netizens protest blockade of Google in the country 
Solidot | 网民大规模抗议Google被封锁 <a href="http://www.solidot.org/story?sid=39919">www.solidot.org/story</a> (2014年06月13日 18:44)
 ---
@thenanfang @cosmopolitanvan @MissXQ @PaulCarsten one small point - it's not a spoof, hoax or deception - it works as Google normally would (2014年06月13日 18:39)
 ---
RT @LYF610400210: 哈哈哈哈哈哈哈……“相关法规”@GreatFireChina <a href="https://twitter.com/LYF610400210/status/477377789297823745/photo/1">twitter.com/LYF610400210/status/477377789297823745/photo/1</a> (2014年06月13日 17:33)
 ---
胜利！ GFW迫于压力对sinaapp.co解封。可以用以下几种方法转发到国内。 sinaapp点co   <a href="http://sinaapp.com">sinaapp.com</a> 访问后去掉m。新浪app点co 新浪=sina  大家自己发挥创造力：） (2014年06月13日 16:50)
 ---
#没有抗争就没有自由 已经成为自由微博上最热门的搜索了。<a href="https://s3-ap-southeast-1.amazonaws.com/freeweibo2/index.html?u=weibo/%E6%B2%A1%E6%9C%89%E6%8A%97%E4%BA%89%E5%B0%B1%E6%B2%A1%E6%9C%89%E8%87%AA%E7%94%B1?censored">s3-ap-southeast-1.amazonaws.com/freeweibo2/index.html</a> 请大家复制到墙内 (2014年06月13日 13:50)
 ---
Google mirror call on Chinese people to fight the GFW. That's us! <a href="http://www.thenanfang.com/blog/google-is-calling-on-chinese-people-to-fight-the-gfw/">www.thenanfang.com/blog/google-is-calling-on-chinese-people-to-fight-the-gfw/</a> (2014年06月13日 13:03)
 ---
@felixonmars 后面这个GFW还没屏蔽。搞了这么多百度新浪的域名，估计GFW头痛不敢屏蔽了 (2014年06月13日 12:48)
 ---
